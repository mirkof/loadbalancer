package com.iptiq

import collection.JavaConverters._
object Main {
  def main(args: Array[String]): Unit = {

    val n = 6
    val providers = (0 until n).map(i => ProviderFactory.generate(i.toString))

    val balancer = new LoadBalancer(LoadBalancer.RoundRobin, 5, 100)
    balancer.register(providers.asJava)

    (0 to 20).foreach { _ =>
      balancer.get()
      Thread.sleep(500)
    }

  }
}