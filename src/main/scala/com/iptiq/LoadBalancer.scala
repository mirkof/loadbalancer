package com.iptiq

import java.util.{Timer, TimerTask}
import java.util
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{ConcurrentHashMap, CopyOnWriteArrayList}

import com.iptiq.LoadBalancer.TrafficDistribution

import collection.JavaConverters._
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class LoadBalancer(algorithm:TrafficDistribution, heartbeatSeconds:Int, parallelism:Int) {
  import LoadBalancer._

  private val registeredProviders = new CopyOnWriteArrayList[Provider]()
  private val activeProviders = new CopyOnWriteArrayList[Provider]()
  private val failedProviders = new ConcurrentHashMap[Provider, Int]()
  private val parallelRequests = new AtomicLong(0)

  def get():String = {
    println(s"LoadBalancer: routing traffic using ${algorithm}")
    val size = activeProviders.size
    if(parallelRequests.get() > size*parallelism) throw new RuntimeException("TooManyRequests")

    val index = algorithm.next(size)
    val provider = activeProviders.get(index)

    parallelRequests.incrementAndGet()
    val result = provider.get()
    parallelRequests.decrementAndGet()
    result
  }

  def register(providers:util.List[Provider]):Int = {
    if((registeredProviders.size() + providers.size()) > MaxSize) throw new IllegalArgumentException(s"Maximum number of instances is $MaxSize")

    registeredProviders.addAll(providers)

    println(s"LoadBalancer: registered ${providers.size()} providers")
    registeredProviders.size()
  }

  def include(provider: Provider):Int = {
    println(s"LoadBalancer: included Provider(${provider.id})")
    val added = registeredProviders.addIfAbsent(provider)

    if(added) 1 else 0
  }

  def exclude(provider: Provider):Int = {
    println(s"LoadBalancer: excluded Provider(${provider.id})")
    val removed = activeProviders.remove(provider)

    if(removed) 1 else 0
  }

  def scheduleHeartbeat():Unit = {
    new Timer().scheduleAtFixedRate(new TimerTask {
      override def run(): Unit = heartbeat()
    }, 0, heartbeatSeconds * 1000)
  }

  private def heartbeat():Unit = {
    println("LoadBalancer: Running heartbeat")
    val registeredSnapshot = registeredProviders.toArray.asInstanceOf[Array[Provider]]
    var actives = new ArrayBuffer[Provider]

      registeredSnapshot.foreach { provider =>
        val alive = provider.check()

        if(alive) {
          val count = failedProviders.getOrDefault(provider, 1)
          if(count == 1) { // passed 2 checks
            failedProviders.remove(provider)
            actives += provider
          } else {
            failedProviders.put(provider, count - 1)
          }

        } else {
          val count = failedProviders.getOrDefault(provider, 1)
          failedProviders.put(provider, count + 1)
        }
    }

    activeProviders.retainAll(actives.asJava)
  }
}

object LoadBalancer {
  final val MaxSize = 10

  sealed trait TrafficDistribution {
    def next(size:Int):Int
    override def toString: String = getClass.getSimpleName.dropRight(1)
  }

  case object RandomDistribution extends TrafficDistribution {
    def next(size:Int):Int = Random.nextInt(size)
  }

  case object RoundRobin extends TrafficDistribution {
    private var current = 0

    def next(size:Int):Int = {
      val index = current
      current = (current + 1) % size
      index
    }
  }
}
