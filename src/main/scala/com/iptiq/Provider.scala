package com.iptiq

import java.util.UUID

import scala.util.Random

object ProviderFactory {
  val failurePercentage:Double = 0


  def generate(id:String = UUID.randomUUID().toString):Provider = {
    val p = new Provider(id)
    println(s"Provider($id) created")
    p
  }
}

class Provider(val id:String) {

  def get():String = {
    println(s"Provider($id) called")
    id
  }

  def check():Boolean = {
    val healthy = Random.nextDouble() > ProviderFactory.failurePercentage
    println(s"""Provider($id) is ${if(healthy) "healthy" else "NOT healthy"}""")
    healthy
  }

}
